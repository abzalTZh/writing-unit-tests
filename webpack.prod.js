const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

module.exports = merge(common, {
    mode: "production",
    optimization: {
      splitChunks: {
          chunks: 'all'
      },
      minimize: true,
      minimizer: [new TerserPlugin(), new CssMinimizerPlugin()]
    },
    devServer: {
      proxy: {
        '/api': 'http://localhost:3000'
      },
      port: 8080
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[id].css",
      }),
    ],
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, "css-loader"],
        },
      ],
    },
  });