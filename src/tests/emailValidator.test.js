import sinon from 'sinon';
import { expect } from 'chai'; 
import { validate, validateAsync, validateWithThrow, validateWithLog } from '../scripts/email-validator';
 
describe('first test', () => { 
  it('should return 2', () => {
    expect(2).to.equal(2);
  });
});

describe('testing the validate function', () => {
  it('should return true when email ends with @gmail.com', () => {
    const expected = true;
    const actual = validate('example@gmail.com');
    expect(actual).to.equal(expected);
  });

  it('should return true when email ends with @outlook.com', () => {
    const expected = true;
    const actual = validate('example@outlook.com');
    expect(actual).to.equal(expected);
  });

  it('should return false in other cases', () => {
    const expected = false;
    const actual = validate('example@example.com');
    expect(actual).to.equal(expected);
  });
});

describe('testing the validateAsync function', () => {
  it('should return true asynchronously when email ends with @gmail.com', async () => {
    const expected = true;
    const actual = await validateAsync('example@gmail.com');
    expect(actual).to.equal(expected);
  });

  it('should return true asynchronously when email ends with @outlook.com', async () => {
    const expected = true;
    const actual = await validateAsync('example@outlook.com');
    expect(actual).to.equal(expected);
  });

  it('should return false in other cases', async () => {
    const expected = false;
    const actual = await validateAsync('example@example.com');
    expect(actual).to.equal(expected);
  });
});

describe('testing the validateWithThrow function', () => {
  it('should return true asynchronously when email ends with @gmail.com', async () => {
    const expected = true;
    const actual = await validateWithThrow('example@gmail.com');
    expect(actual).to.equal(expected);
  });

  it('should return true asynchronously when email ends with @outlook.com', async () => {
    const expected = true;
    const actual = await validateWithThrow('example@outlook.com');
    expect(actual).to.equal(expected);
  });

  it('should throw an error in other cases', () => {
    const expected = "Email is invalid!";
    const actual = function() { validateWithThrow('example@example.com'); };
    expect(actual).to.throw(expected);
  });
});

describe('testing the validateWithLog function', () => {
  it('should log and return true when email ends with @gmail.com', () => {
    const stub = sinon.stub(console, 'log');
    const expected = true;
    const actual = validateWithLog('example@gmail.com');

    expect(console.log.calledWith(true)).to.be.true;
    expect(actual).to.equal(expected);

    stub.restore();
  });

  it('should log and return true when email ends with @outlook.com', () => {
    const stub = sinon.stub(console, 'log');
    const expected = true;
    const actual = validateWithLog('example@outlook.com');

    expect(console.log.calledWith(true)).to.be.true;
    expect(actual).to.equal(expected);

    stub.restore();
  });

  it('should log and return false in other cases', () => {
    const stub = sinon.stub(console, 'log');
    const expected = false;
    const actual = validateWithLog('example@example.com');

    expect(console.log.calledWith(false)).to.be.true;
    expect(actual).to.equal(expected);

    stub.restore();
  });
})