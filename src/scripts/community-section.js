class SectionCreater2 {
    create() {
        return new CommunitySection().constructSection();
    }
}

class CommunitySection {
    constructSection() {
        const parentSection = document.querySelector('#sectionHeadline');

        const commSection = document.createElement('section');
        commSection.classList.add('app-section');
        commSection.classList.add('app-section--community');
        parentSection.parentNode.insertBefore(commSection, parentSection.nextSibling);

        const sectionHeader = document.createElement('h2');
        sectionHeader.className = 'app-title';
        const headerContent1 = document.createTextNode('Big Community of');
        const headerContent2 = document.createTextNode('People Like You');

        sectionHeader.appendChild(headerContent1);
        sectionHeader.appendChild(document.createElement('br'));
        sectionHeader.appendChild(headerContent2);
        commSection.appendChild(sectionHeader);

        const sectionSubtitle = document.createElement('h3');
        sectionSubtitle.className = 'app-subtitle';
        const subtitleContent1 = document.createTextNode('We’re proud of our products, and we’re really excited when');
        const subtitleContent2 = document.createTextNode('we get feedback from our users.');

        sectionSubtitle.appendChild(subtitleContent1);
        sectionSubtitle.appendChild(document.createElement('br'));
        sectionSubtitle.appendChild(subtitleContent2);
        commSection.appendChild(sectionSubtitle);

        const usersContainer = document.createElement('div');
        usersContainer.classList.add('app-section__users-container');
        commSection.appendChild(usersContainer);

        const user1 = document.createElement('div');
        const user2 = document.createElement('div');
        const user3 = document.createElement('div');
        user1.classList.add('app-section__user');
        user2.classList.add('app-section__user');
        user3.classList.add('app-section__user');
        usersContainer.appendChild(user1);
        usersContainer.appendChild(user2);
        usersContainer.appendChild(user3);

        const avi1 = document.createElement('img');
        const avi2 = document.createElement('img');
        const avi3 = document.createElement('img');
        avi1.classList.add('app-section__user-avi');
        avi2.classList.add('app-section__user-avi');
        avi3.classList.add('app-section__user-avi');

        const desc1 = document.createElement('p');
        const desc2 = document.createElement('p');
        const desc3 = document.createElement('p');
        desc1.classList.add('app-section__user-desc');
        desc2.classList.add('app-section__user-desc');
        desc3.classList.add('app-section__user-desc');

        const name1 = document.createElement('p');
        const name2 = document.createElement('p');
        const name3 = document.createElement('p');
        name1.classList.add('app-section__user-name');
        name2.classList.add('app-section__user-name');
        name3.classList.add('app-section__user-name');

        const position1 = document.createElement('p');
        const position2 = document.createElement('p');
        const position3 = document.createElement('p');
        position1.classList.add('app-section__user-pos');
        position2.classList.add('app-section__user-pos');
        position3.classList.add('app-section__user-pos');

        user1.appendChild(avi1);
        user1.appendChild(desc1);
        user1.appendChild(name1);
        user1.appendChild(position1);

        user2.appendChild(avi2);
        user2.appendChild(desc2);
        user2.appendChild(name2);
        user2.appendChild(position2);

        user3.appendChild(avi3);
        user3.appendChild(desc3);
        user3.appendChild(name3);
        user3.appendChild(position3);

        fetch('http://localhost:3000/community')
            .then((response) => response.json())
            .then((data) => {
                console.log(data);

                avi1.setAttribute('src', data[0].avatar);
                desc1.textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
                name1.textContent = `${data[0].firstName} ${data[0].lastName}`;
                position1.textContent = data[0].position;

                avi2.setAttribute('src', data[1].avatar);
                desc2.textContent = 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.';
                name2.textContent = `${data[1].firstName} ${data[1].lastName}`;
                position2.textContent = data[1].position;

                avi3.setAttribute('src', data[2].avatar);
                desc3.textContent = 'Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
                name3.textContent = `${data[2].firstName} ${data[2].lastName}`;
                position3.textContent = data[2].position;
            })
            .catch((err) => console.log(err));
    }
}

export { SectionCreater2 };