const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export function validate (email) {
  if (email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1])) {
    return true;
  } else return false;
}

export async function validateAsync (email) {
  if (email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1])) {
    return Promise.resolve(true);
  } else return Promise.resolve(false);
}

export function validateWithThrow (email) {
  if (email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1])) {
    return true;
  } else { throw new Error('Email is invalid!') };
}

export function validateWithLog (email) {
  if (email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1])) {
    console.log(true);
    return true;
  } else {
    console.log(false);
    return false
  };
}